# nodejs blockchain barebones provenance client #

## Getting Started ##
This application demonstrates Proof of Provenance using the Mastercard Core Blockchain API. To get started you should take the following steps 

 * Clone this repository
 * Edit the message.proto file and assign your APP_ID for value of package
 * Goto Mastercard Developers and create a Mastercard Blockchain project (note this is currently a private API and you may need to request access). You will be taken through the wizard to create a node. You must provide an APP_ID and a protocol buffer definition i.e. message.proto.
 * You will receive a p12 file and a consumer key from Mastercard Developers for your project.
 * Execute the following commands
```bash
npm install
node app.js
```
When started it gets you to confirm your parameters and then displays a simple menu. 

## Usage

### Create Users

Create a new user marked as an issuing authority. Products can be created and issued to the blockchain only by users marked as authorities.

Create other users to represent merchants and consumers.

### Create a Product

Create a product and associate it with the issuing authority user.
The product can then be transferred to other users.

### Authenticate a Product

Product ownership can be verified by specifying the product and user to verify ownership against.

## Caution

It may be necessary to manually create a node instance on the blockchain if it doesn't get created on the developer portal. This can be done from the app by selecting option 8. 

## Menu ##
```
============ MENU ============
1. Create user
2. List users
3. Create product
4. List products
5. Authenticate product
6. Transfer product
7. Print Command Line Options
8. Provision instance
9. Reset datastore
10. Update instance
0. Quit
Option:  (0)
```

You should be able to create and read entries and transactions at will, so long as you update with the location of your buffer definitions in between.

## More Commandline Options ##
```
Options:
  --help                Show help                                      [boolean]
  --version             Show version number                            [boolean]
  --consumerKey, --ck   consumer key (mastercard developers)
  --keystorePath, --kp  the path to your keystore (mastercard developers)
  --keyAlias, --ka      key alias (mastercard developers)
  --storePass, --sp     keystore password (mastercard developers)
  --protoFile, --pf     protobuf file
  --verbosity, -v       log mastercard developers sdk to console[default: false]
```

### Useful Info
This project makes use of the Mastercard Blockchain SDK available from npm.

```bash
npm install mastercard-blockchain --save
```
