#!/usr/bin/env node
var blockchain = require('mastercard-blockchain');
var MasterCardAPI = blockchain.MasterCardAPI;

const async = require('async'), encoding = 'base64', fs = require('fs'), chain = require('nodejs-blockchain-crypto-tools').chain(), pki = require('nodejs-blockchain-crypto-tools').pki();
var prompt = require('prompt'), options = createOptions();

var protobuf = require("protobufjs");

var argv = options.argv;
prompt.override = argv;

var appID = null;
var msgClassDef = null;
var productIssuanceDef = null;
var productTransferDef = null;
var protoFile = null;
var protoFileContent = null;
var dataFile = "data.json";
var datastore = {
  users: [],
  products: []
};

readDataFile();
console.log(fs.readFileSync('help.txt').toString());
prompt.start();
initApi((err, result) => {
  if (!err) {
    updateNode((error, data) => {
      processCommands();
    });
  }
});

function processCommands() {
  async.waterfall([
    function (callback) {
      prompt.get(promptCmdSchema(), callback);
    }
  ], function (err, result) {
    if (!err) {
      switch (result.cmdOption) {
        case 1:
          createUser((error, data) => {
            processCommandsAfter();
          });
          break;
        case 2:
          listUsers((error, data) => {
            processCommandsAfter();
          });
          break;
        case 3:
          createProduct((error, data) => {
            processCommandsAfter();
          });
          break;
        case 4:
          listProducts((error, data) => {
            processCommandsAfter();
          });
          break;
        case 5:
          authenticateProduct((error, data) => {
            processCommandsAfter();
          });
          break;
        case 6:
          transferProduct((error, data) => {
            processCommandsAfter();
          });
          break;
        case 7:
          options.showHelp();
          async.nextTick(processCommandsAfter);
          break;
        case 8:
          createNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 9:
          datastore = {
            users: [],
            products: []
          };
          writeDataFile();
          async.nextTick(processCommandsAfter);
          break;
        case 10:
          updateNode((error, data) => {
            processCommandsAfter();
          });
          break;
        default:
          console.log('Goodbye');
          break;
      }
    }
  });
}

function createUser(callback) {
  console.log('createUser');
  async.waterfall([
    function (callback) {
      prompt.get(promptUserSchema(), callback);
    }
  ], function (err, data) {
    if (err) {
      console.log('error', err);
      callback(err, null);
    } else {
      datastore.users.push({ address: chain.generateAddress(chain.addressPrefix.entity), name: data.name, authority: data.authority });
      writeDataFile();
      console.log('User created');
      callback(null, 'User created');
    }
  });
}

function listUsers(callback) {
  console.log('listUsers');
  datastore.users.forEach((usr, idx) => { console.log(idx + 1, '.', usr.name, 'Authority', usr.authority); });
  async.nextTick(callback, null, {});
}

function listProducts(callback) {
  console.log('listProducts');
  datastore.products.forEach((prd, idx) => { console.log(idx + 1, '.', prd.name, 'hash', prd.hash, 'manufacturerReference', prd.manufacturerReference, 'transfers', prd.transfers.length); });
  async.nextTick(callback, null, {});
}

function createProduct(callback) {
  console.log('createProduct');
  var product = null;
  async.waterfall([
    function (callback) {
      prompt.get(promptProductSchema(), callback);
    },
    function (data, callback) {
      product = { address: chain.generateAddress(chain.addressPrefix.entity), name: data.name, manufacturerReference: data.manuRef, transfers: [] };
      var authority = getAuthorities()[data.authority - 1];
      var productIssuance = {
        timestamp: new Date().getTime(),
        issuerAddress: authority.address.id,
        product: [
          {
            productAddress: product.address.id,
            manufacturerLocatorId: Array.prototype.slice.call(new Buffer(product.manufacturerReference), 0)
          }
        ]
      };
      var signature = pki.sign(productIssuanceDef.encode(productIssuanceDef.create(productIssuance)).finish(), authority.address.privateKeyPem);
      productIssuance.issuerSignature = signature;
      var payload = { type: 0, payload: productIssuanceDef.encode(productIssuance).finish() };
      var errMsg = msgClassDef.verify(payload);
      if (errMsg) {
        callback(errMsg, null);
      } else {
        var message = msgClassDef.create(payload);
        blockchain.TransactionEntry.create({
          "app": appID,
          "encoding": encoding,
          "value": msgClassDef.encode(message).finish().toString(encoding)
        }, callback);
      }
    },
    function (resp, callback) {
      waitForConfirmation(resp.hash, 3000, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      product.hash = result.hash;
      datastore.products.push(product);
      writeDataFile();
      console.log('Product Created');
    }
    async.nextTick(callback, err, result);
  });
}

function waitForConfirmation(hashToFind, sleeptime, callback) {
  retrieveEntry('waiting for confirmation', (err, resp, decoded) => {
    if ('confirmed' === resp.status) {
      callback(null, resp);
    } else {
      async.nextTick(() => {
        setTimeout(() => {
          waitForConfirmation(hashToFind, sleeptime, callback);
        }, sleeptime);
      });
    }
  }, hashToFind);
}

function authenticateProduct(callback) {
  console.log('authenticateProduct');
  var product = null;
  var user = null;
  async.waterfall([
    function (callback) {
      prompt.get(promptAuthenticateSchema(), callback);
    },
    function (data, callback) {
      product = datastore.products[data.product - 1];
      user = datastore.users[data.user - 1];
      retrieveEntry('checking product', callback, product.hash);
    },
    function (resp, decoded, callback) {
      var message = productIssuanceDef.decode(new Buffer(decoded.payload, encoding));
      var object = productIssuanceDef.toObject(message, {
        longs: String,
        enums: String,
        bytes: String
      });
      if (0 === product.transfers.length) {
        callback(null, [product.name, 'belongs to', user.name, '->', user.address.id === object.issuerAddress].join(' '));
      } else {
        checkInTransfers([].concat(product.transfers), product, user, callback);
      }
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function checkInTransfers(transfers, product, user, callback) {
  if (0 < transfers.length) {
    async.waterfall([
      function (callback) {
        retrieveEntry('checking in transfers', callback, transfers[transfers.length - 1].hash);
      }
    ], function (err, result, decoded) {
      if (err) {
        console.log('error', err);
      } else {
        var message = productTransferDef.decode(new Buffer(decoded.payload, encoding));
        var object = productTransferDef.toObject(message, {
          longs: String,
          enums: String,
          bytes: String
        });
      }
      async.nextTick(callback, err, [product.name, 'belongs to', user.name, '->', user.address.id === object.nextOwnerAddress].join(' '));
    });

  } else {
    async.nextTick(callback, 'empty transfers');
  }
}

function transferProduct(callback) {
  console.log('transferProduct');
  var product = null;
  var user = null;
  var usingTransfers = false;
  async.waterfall([
    function (callback) {
      prompt.get(promptTransferSchema(), callback);
    },
    function (data, callback) {
      product = datastore.products[data.product - 1];
      user = datastore.users[data.user - 1];
      if (0 == product.transfers.length) {
        retrieveEntry('checking product', callback, product.hash);
      } else {
        usingTransfers = true;
        retrieveEntry('checking transfers', callback, product.transfers[product.transfers.length - 1].hash);
      }
    },
    function (resp, decoded, callback) {
      var decoderDef = usingTransfers ? productTransferDef : productIssuanceDef;
      var message = decoderDef.decode(new Buffer(decoded.payload, encoding));
      var object = decoderDef.toObject(message, {
        longs: String,
        enums: String,
        bytes: String
      });
      var currentOwner = datastore.users.find((usr) => { return usingTransfers ? usr.address.id === object.nextOwnerAddress : usr.address.id === object.issuerAddress; });
      var productTransfer = {
        timestamp: new Date().getTime(),
        previousTransfer: new Buffer(resp.hash),
        productAddress: product.address.id,
        nextOwnerAddress: user.address.id
      };
      productTransfer.ownerSignature = pki.sign(productTransferDef.encode(productTransferDef.create(productTransfer)).finish(), currentOwner.address.privateKeyPem);
      productTransfer.ownerPublicKey = currentOwner.address.publicKey;
      var payload = { type: 1, payload: productTransferDef.encode(productTransfer).finish() };
      var errMsg = msgClassDef.verify(payload);
      if (errMsg) {
        callback(errMsg, null);
      } else {
        var message = msgClassDef.create(payload);
        blockchain.TransactionEntry.create({
          "app": appID,
          "encoding": encoding,
          "value": msgClassDef.encode(message).finish().toString(encoding)
        }, callback);
      }
    },
    function (resp, callback) {
      waitForConfirmation(resp.hash, 3000, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      product.transfers.push({ hash: result.hash, productAddress: product.address.id, slot: result.slot });
      writeDataFile();
      console.log('Product Transfered');
    }
    async.nextTick(callback, err, result);
  });
}

function updateNode(callback) {
  console.log('updateNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'the path to the protobuf File',
        default: 'message.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + ".PaxmMessage");
        productIssuanceDef = root.lookupType(appID + ".ProductIssuance");
        productTransferDef = root.lookupType(appID + ".ProductTransfer");
        blockchain.App.update({
          id: appID,
          name: appID,
          description: "",
          version: 0,
          definition: {
            format: "proto3",
            encoding: encoding,
            messages: fs.readFileSync(protoFile).toString(encoding)
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    },
    function (result, callback) {
      blockchain.App.read(appID, {}, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function createNode(callback) {
  console.log('createNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'the path to the protobuf File',
        default: 'message.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + ".PaxmMessage");
        productIssuanceDef = root.lookupType(appID + ".ProductIssuance");
        productTransferDef = root.lookupType(appID + ".ProductTransfer");
        blockchain.Node.provision({
          network: 'ZONE',
          application: {
            name: appID,
            description: "",
            version: 0,
            definition: {
              format: "proto3",
              encoding: 'base64',
              messages: fs.readFileSync(protoFile).toString('base64')
            }
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function retrieveEntry(title, callback, hash) {
  console.log(title);
  var ctx = {};
  async.waterfall([
    function (callback) {
      if (hash) {
        async.nextTick(callback, null, { hash: hash });
      } else {
        prompt.get(['hash'], callback);
      }
    },
    function (data, callback) {
      blockchain.TransactionEntry.read("", { "hash": data.hash }, callback);
    },
    function (data, callback) {
      ctx.data = data;
      var message = msgClassDef.decode(new Buffer(data.value, 'hex'));
      var object = msgClassDef.toObject(message, {
        longs: String,
        enums: String,
        bytes: String
      });
      callback(null, object);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else if (!hash) {
      console.log('response', ctx.data, 'decoded', result);
    }
    async.nextTick(callback, err, ctx.data, result);
  });
}


function initApi(onDone) {
  console.log('initializing');
  async.waterfall([
    function (callback) {
      prompt.get(promptInitSchema(), callback);
    },
    function (result, callback) {
      var authentication = new MasterCardAPI.OAuth(result.consumerKey, result.keystorePath, result.keyAlias, result.storePass);
      MasterCardAPI.init({
        sandbox: true,
        debug: argv.verbosity,
        authentication: authentication
      });
      protoFile = result.protoFile;
      protobuf.load(protoFile, callback);
    }
  ], function (err, root) {
    if (err) {
      console.log('error', err);
    } else {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + ".PaxmMessage");
        productIssuanceDef = root.lookupType(appID + ".ProductIssuance");
        productTransferDef = root.lookupType(appID + ".ProductTransfer");
        console.log('initialized');
      } else {
        console.log('could not read message class def from', protoFile);
      }
    }
    async.nextTick(onDone, err, appID);
  });
}

function processCommandsAfter() {
  async.waterfall([
    function (callback) {
      prompt.get({ properties: { enter: { description: 'press enter to continue', required: false } } }, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    }
    async.nextTick(processCommands);
  });
}

function promptInitSchema() {
  return {
    properties: {
      keystorePath: {
        description: 'the path to your keystore (mastercard developers)',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      },
      storePass: {
        description: 'keystore password (mastercard developers)',
        required: true,
        default: 'keystorepassword'
      },
      consumerKey: {
        description: 'consumer key (mastercard developers)',
        required: true
      },
      keyAlias: {
        description: 'key alias (mastercard developers)',
        required: true,
        default: 'keyalias'
      },
      protoFile: {
        description: 'the path to the protobuf File',
        required: true,
        default: 'message.proto'
      }
    }
  };
}

function promptUserSchema() {
  return {
    properties: {
      name: {
        description: 'Name',
        required: true,
        default: 'John Doe'
      },
      authority: {
        description: 'Is Authority',
        required: true,
        type: 'boolean',
        default: false
      }
    }
  };
}

function promptProductSchema() {
  var authorities = getAuthorities();
  return {
    properties: {
      name: {
        description: 'Name',
        required: true,
        default: 'Diamond'
      },
      manuRef: {
        description: 'Manufacturer Reference',
        required: true,
        default: 'mref:1234'
      },
      authority: {
        description: 'Select Authority (needs a user with authority role): \n' + authorities.map((entry, idx) => { return (idx + 1) + '.' + entry.name }).join('\n') + '\n',
        message: 'Invalid Option',
        type: 'number',
        required: true,
        conform: (value) => {
          return value > 0 && value <= authorities.length;
        }
      }
    }
  };
}

function promptAuthenticateSchema() {
  return {
    properties: {
      product: {
        description: 'Select Product to authenticate\n' + datastore.products.map((entry, idx) => { return (idx + 1) + '.' + entry.name }).join('\n') + '\n',
        message: 'Invalid Option',
        type: 'number',
        required: true,
        conform: (value) => {
          return value > 0 && value <= datastore.products.length;
        }
      },
      user: {
        description: 'Select User that should be owner\n' + datastore.users.map((entry, idx) => { return (idx + 1) + '.' + entry.name }).join('\n') + '\n',
        message: 'Invalid Option',
        type: 'number',
        required: true,
        conform: (value) => {
          return value > 0 && value <= datastore.users.length;
        }
      }
    }
  };
}

function promptTransferSchema() {
  return {
    properties: {
      product: {
        description: 'Select product\n' + datastore.products.map((entry, idx) => { return (idx + 1) + '.' + entry.name }).join('\n') + '\n',
        message: 'Invalid Option',
        type: 'number',
        required: true,
        conform: (value) => {
          return value > 0 && value <= datastore.products.length;
        }
      },
      user: {
        description: 'Select new owner\n' + datastore.users.map((entry, idx) => { return (idx + 1) + '.' + entry.name }).join('\n') + '\n',
        message: 'Invalid Option',
        type: 'number',
        required: true,
        conform: (value) => {
          return value > 0 && value <= datastore.users.length;
        }
      }
    }
  };
}

function promptCmdSchema() {
  return {
    properties: {
      cmdOption: {
        description: '\n============ MENU ============\n1. Create user\n2. List users\n3. Create product\n4. List products\n5. Authenticate product\n6. Transfer product\n7. Print Command Line Options\n8. Provision instance\n9. Reset datastore\n10. Update instance\n0. Quit\nOption',
        message: 'Invalid Option',
        type: 'number',
        default: 0,
        required: true,
        conform: (value) => {
          return value >= 0 && value <= 10;
        }
      }
    }
  };
}

function createOptions() {
  return require('yargs')
    .options({
      'consumerKey': {
        alias: 'ck',
        description: 'consumer key (mastercard developers)'
      },
      'keystorePath': {
        alias: 'kp',
        description: 'the path to your keystore (mastercard developers)'
      },
      'keyAlias': {
        alias: 'ka',
        description: 'key alias (mastercard developers)'
      },
      'storePass': {
        alias: 'sp',
        description: 'keystore password (mastercard developers)'
      },
      'protoFile': {
        alias: 'pf',
        description: 'protobuf file'
      },
      'verbosity': {
        alias: 'v',
        default: false,
        description: 'log mastercard developers sdk to console'
      }
    });
}

function getProperties(obj) {
  var ret = [];
  for (var name in obj) {
    if (obj.hasOwnProperty(name)) {
      ret.push(name);
    }
  }
  return ret;
}

function guessNested(root) {
  var props = getProperties(root.nested);
  var firstChild = getProperties(root.nested[props[0]].nested);
  return [props[0], firstChild[0]];
}

function getAuthorities() {
  return datastore.users.filter((entry) => { return entry.authority; });
}

function readDataFile() {
  if (fs.existsSync(dataFile)) {
    datastore = JSON.parse(fs.readFileSync(dataFile).toString());
  }
}

function writeDataFile() {
  fs.writeFileSync(dataFile, JSON.stringify(datastore));
}

